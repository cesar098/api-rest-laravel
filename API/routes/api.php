<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/cuenta','App\Http\Controllers\CuenteController@index');
Route::post('/cuenta','App\Http\Controllers\CuenteController@store');
Route::put('/cuenta/{id}','App\Http\Controllers\CuenteController@update');
Route::delete('/cuenta/{id}','App\Http\Controllers\CuenteController@destroy');

Route::get('/cliente','App\Http\Controllers\ClienteController@index');
Route::get('/cliente/{id}','App\Http\Controllers\ClienteController@show');
Route::post('/cliente','App\Http\Controllers\ClienteController@store');
Route::post('/cliente/{id}','App\Http\Controllers\ClienteController@update');
Route::delete('/cliente/{id}','App\Http\Controllers\ClienteController@destroy');

Route::get('/transaccion','App\Http\Controllers\TransaccionController@index');
Route::post('/transaccion','App\Http\Controllers\TransaccionController@store');
Route::put('/transaccion/{id}','App\Http\Controllers\TransaccionController@update');
Route::delete('/transaccion/{id}','App\Http\Controllers\TransaccionController@destroy');
